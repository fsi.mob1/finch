#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import glob
import os
import re
import shlex
import shutil
import subprocess
import sys
import threading
import time
import tempfile
import zipfile
import datetime
import queue

from loguru import logger as log
import click
import requests
import sentry_sdk
from sentry_sdk import serializer
import paramiko

import util
from util import start_fastboot_mode


# Monkeypatching: trace more than 10 local variables
serializer.MAX_DATABAG_BREADTH = 20
sentry_dsn = os.environ.get('SENTRY_DSN')
if sentry_dsn:
    if shutil.which('git'):
        context = {
            'name': 'finch',
            'head': subprocess.check_output([
                'git', 'symbolic-ref', 'HEAD'
            ]).decode().strip(),
            'revision': subprocess.check_output([
                'git', 'rev-parse', 'HEAD'
            ]).decode().strip(),
        }
        sentry_sdk.set_context('app', context)

    sentry_sdk.init(sentry_dsn, traces_sample_rate=1.0)


EXTERNAL_PROGRAMS = [
    'adb', 'fastboot',  # Android manager
    'cpio', 'atool', 'aunpack', 'apack',  # for managing image archives
]
FLASH_IMAGE_PATH = 'factory_image'
FLASH_IMAGE_ALIAS = {
    'crosshatch': {
        '9': 'pq2a.190305.002',
    }
}


@click.group()
def cli():
    pass


# TODO: implement scale up / down


def resolve_version_alias(device_code, version):
    try:
        version = FLASH_IMAGE_ALIAS[device_code][version]
        return version
    except KeyError:
        return version


def prepare_flash_image(device_code, version):
    version = resolve_version_alias(device_code, version)

    local_path = ''
    res = glob.glob(os.path.join(
        FLASH_IMAGE_PATH, f'{device_code}-{version}-factory-*.zip'))
    if len(res) == 0:
        # Try to download image zip file
        url, file_hash = util.get_android_image_url(device_code, version)
        assert url

        log.info('Download android image from url %r' % (url,))
        now = time.time()
        filename = os.path.split(url.split('?')[0])[1]
        local_path = os.path.join(FLASH_IMAGE_PATH, filename)
        assert util.download_file(url, local_path, file_hash) > 0
        elapsed = time.time() - now
        log.debug('Elapsed %.3f seconds to download' % (elapsed,))
    else:
        local_path = res[0]

    # extract bootimg from stock rom
    unpack_path = os.path.join(FLASH_IMAGE_PATH, f'{device_code}-{version}')
    if not os.path.isdir(unpack_path):
        log.debug('Unpacking')
        zipfile.ZipFile(local_path).extractall(FLASH_IMAGE_PATH)
        zipfile.ZipFile(
            os.path.join(unpack_path, f'image-{device_code}-{version}.zip')
        ).extract('boot.img', unpack_path)

    return unpack_path


@cli.group()
def prepare():
    pass


@prepare.command('kernel')
@click.option('-d', '--device', 'device_code', default='crosshatch',
              help='Device code (ex: Google Pixel 3XL = crosshatch)')
@click.option('-v', '--version', 'version', default='9',
              help='Android version to make build (ex: 9)')
@click.option('-k', '--kernel', 'kernel', default='',
              help='new Image (ex. Image.lz4-dtb)')
def cmd_prepare_kernel(device_code, version, kernel):
    _cmd_prepare_kernel_real(device_code, version, kernel)


def _cmd_prepare_kernel_real(device_code, version, kernel):
    unpack_path = prepare_flash_image(device_code, version)

    orig_bootimg_path = os.path.abspath(os.path.join(unpack_path, 'boot.img'))
    assert os.path.exists(orig_bootimg_path)
    mkbootimg_path = os.path.abspath(
        os.path.join('bin', 'mkbootimg.'+sys.platform))
    unmkbootimg_path = os.path.abspath(
        os.path.join('bin', 'unmkbootimg.'+sys.platform))

    if not kernel:
        log.info('Kernel path not specified; using predefined one...')
        kernel = os.path.join('prebuilt', device_code,
                              resolve_version_alias(device_code, version),
                              'kernel')

    assert os.path.exists(kernel)

    oldpwd = os.getcwd()
    kernel_abs = os.path.join(oldpwd, kernel)
    with tempfile.TemporaryDirectory() as td:
        os.chdir(td)

        # unpack original boot.img -> kernel, ramdisk, mkbootimg cmdline
        output = subprocess.check_output([
            unmkbootimg_path, '-i', orig_bootimg_path])
        build_cmd = shlex.split(
            output.decode().strip().split('\n')[-1].strip())

        # compare kernel version, and if mismatched show warning
        orig_kernel_version = util.extract_linux_version('kernel')
        my_kernel_version = util.extract_linux_version(kernel_abs)
        if orig_kernel_version != my_kernel_version:
            log.warning('Kernel version mismatched: orig(%s) != new(%s)' % (
                orig_kernel_version, my_kernel_version))

        # build with new kernel
        build_cmd[build_cmd.index('--kernel') + 1] = kernel_abs
        build_cmd[build_cmd.index('-o') + 1] = 'boot_new.img'
        build_cmd[0] = mkbootimg_path
        subprocess.check_call(build_cmd)

        # move new boot.img to somewhere
        output_path = os.path.join(oldpwd, 'output', 'boot_new.img')
        os.rename('boot_new.img', output_path)
        log.info('Build new boot.img in %r' % (output_path,))

    os.chdir(oldpwd)


@prepare.command('platform')
@click.option('-u', '--url', 'url', default='finch@AOSP:system.img.raw',
              help='SFTP connection string to download system.img. '
                   '(ex: finch@AOSP:system.img.raw)')
@click.option('-f', '--force', 'force', default=False,
              help='Overwrite platform image if exists')
def cmd_prepare_platform(url, force):
    local_path = os.path.join('output', 'system.img.raw')
    if os.path.exists(local_path) and not force:
        log.info('Platform file already exists.')
        return

    match = re.match(r'([^@]+)@([^:]+):(.*)', url)
    if not match:
        log.error('Invalid SFTP connector provided.')
        return

    username, host, path = match.groups()

    cli = paramiko.SSHClient()
    cli.load_system_host_keys()
    cli.connect(host, username=username)
    sftp = cli.open_sftp()

    log.info('Downloading file from %r' % (url,))
    now = time.time()
    sftp.get(path, local_path)
    elapsed = time.time() - now
    log.info('Elapsed %.3f seconds' % (elapsed,))

    sftp.close()
    cli.close()


@cli.group()
def device():
    pass


@device.command('reset')
@click.option('-d', '--device', 'device_code', default='crosshatch',
              help='Device code (ex: Google Pixel 3XL = crosshatch)')
@click.option('-v', '--version', 'version', default='9',
              help='Android version to make build (ex: 9)')
@click.option('-s', '--serial', 'serial', default='')
def cmd_device_reset(device_code, version, serial):
    unpack_path = prepare_flash_image(device_code, version)
    start_fastboot_mode(serial)

    oldpwd = os.getcwd()
    os.chdir(unpack_path)
    os.system("sh ./flash-all.sh")
    os.chdir(oldpwd)


@device.group()
def flash():
    pass


@flash.command('kernel')
@click.option('-b', '--boot', 'boot_img_path', default='',
              help='path of new boot.img')
@click.option('-m', '--module-dir', 'module_dir', default='',
              help='path of kernel modules to autoload on boot')
@click.option('-d', '--device', 'device_code', default='crosshatch',
              help='Device code (ex: Google Pixel 3XL = crosshatch)')
@click.option('-v', '--version', 'version', default='9',
              help='Android version to make build (ex: 9)')
@click.option('-s', '--serial', 'serial', default='')
def cmd_device_flash_kernel(boot_img_path, module_dir, device_code, version,
                            serial):
    if boot_img_path == '' and module_dir == '':
        log.info('No path arguments specified; using predefined one...')
        version = resolve_version_alias(device_code, version)
        _cmd_prepare_kernel_real(device_code, version, kernel='')

        # FIXME: hardcoded
        boot_img_path = os.path.join('output', 'boot_new.img')
        module_dir = os.path.join('prebuilt', device_code, version,
                                  'drivers')
        log.info('boot_img_path=%r' % (boot_img_path,))
        log.info('module_dir=%r' % (module_dir,))

    assert os.path.exists(boot_img_path)
    assert os.path.isdir(module_dir)

    # install kernel
    log.info('Entering fastboot')
    serial = start_fastboot_mode(serial)
    log.info('Flashing kernel')
    subprocess.check_call(['fastboot', '-s', serial, 'flash', 'boot',
                           boot_img_path])
    subprocess.check_call(['fastboot', '-s', serial, 'reboot'])

    # install driver
    log.info('Waiting for boot')
    subprocess.check_call(['adb', '-s', serial, 'wait-for-device'])
    while True:
        res = subprocess.check_output(['adb', '-s', serial, 'shell',
                                       'getprop', 'sys.boot_completed'])
        if res.strip() == b'1':
            break

        time.sleep(1)

    log.info('Importing kernel modules')
    for module in glob.glob(os.path.join(module_dir, '*.ko')):
        subprocess.check_call(['adb', 'push', module, '/data/local/tmp/'])
        subprocess.check_call([
            'adb', 'shell', 'sh', '-c',
            "/system/bin/mv\\ /data/local/tmp/%s\\ /vendor/lib/modules" % (
                os.path.split(module)[-1],)
        ])

    log.info('Rebooting')
    subprocess.check_call(['adb', 'reboot'])
    log.info('Done! let\'s grep HOOKA :)')


@flash.command('platform')
@click.option('-p', '--platform', 'platform_path',
              default=os.path.join('output', 'system.img.raw'))
@click.option('-s', '--serial', 'serial', default='')
def cmd_device_flash_platform(platform_path, serial):
    assert os.path.exists(platform_path)
    serial = start_fastboot_mode(serial)
    subprocess.check_call(['fastboot', '-s', serial, 'flash', 'system',
                           platform_path])
    subprocess.check_call(['fastboot', '-s', serial, 'reboot'])


def _produce_log_logcat(stop_event: threading.Event,
                        serial: str, q: queue.Queue,
                        filter_uid: int):
    cmdline = ['adb']
    if serial:
        cmdline += ['-s', serial]

    cmdline += ['logcat', '-v', 'long']
    p = subprocess.Popen(cmdline, stdout=subprocess.PIPE, bufsize=0)

    buffer = b''
    delim = b'\n\n'
    logger_name = ''
    head_reg = re.compile(
        r'\[ (\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2}).(\d{3})[ ]+(\d+):[ ]*(\d+) (.)\/(.+)+\]')  # noqa

    while True:
        if stop_event.is_set():
            break

        buffer += p.stdout.read(1)
        while delim in buffer:
            chunk = buffer[:buffer.index(delim)]
            buffer = buffer[len(chunk) + len(delim):]
            chunk = chunk.lstrip()
            data = {}

            # Parse logger name
            if chunk.startswith(b'--------- beginning of '):
                line, chunk = chunk.split(b'\n', 1)
                logger_name = line.decode().strip().split()[-1]

            data['logger'] = logger_name

            # Parse header
            if b'\n' not in chunk:
                continue

            try:
                head, chunk = chunk.split(b'\n', 1)
                match = head_reg.match(head.decode())
                if not match:
                    log.warning('Parsing head failed: head=%r' % (head,))
                    continue

                groups = match.groups()
                data['datetime'] = datetime.datetime(
                    year=datetime.datetime.now().year,
                    month=int(groups[0]),
                    day=int(groups[1]),
                    hour=int(groups[2]),
                    minute=int(groups[3]),
                    second=int(groups[4]),
                    microsecond=int(groups[5])
                ).timestamp()
                data['pid'] = int(groups[6])
                data['tid'] = int(groups[7])
                data['level'] = groups[8]
                data['tag'] = groups[9].strip()

                message = chunk.decode('unicode_escape')
                match = re.match(r'\[(\d+)] \[([^]]+)] \[([^]]*)]', message)
                assert match

                uid, caller, payload = match.groups()
                uid = int(uid)
                data['uid'] = uid
                data['caller'] = caller
                data['payload'] = payload

                if uid != filter_uid:
                    continue

                q.put(data)

            # drop invalid log contents
            except (UnicodeDecodeError, DeprecationWarning,  # chunk.decode
                    AssertionError,  # message format validation
                    ValueError):  # int(uid)
                continue

    p.kill()
    stop_event.set()


def _produce_log_logcat_sender(stop_event: threading.Event,
                               session_id: int, sess: requests.Session,
                               url: str, q: queue.Queue):
    interval = 1
    endpoint = url + '/api/log/push?id=%s' % (session_id,)

    def _bulk_push(_sess: requests.Session, _endpoint: str, q: queue.Queue):
        logs = []
        while True:
            try:
                logs.append(q.get_nowait())
            except queue.Empty:
                break

        if logs:
            _sess.put(_endpoint, json=logs)

        return len(logs)

    while True:
        log_count = _bulk_push(sess, endpoint, q)
        if log_count > 0:
            log.debug('_produce_log_logcat_sender: sent %d logs' % (log_count,))

        if stop_event.is_set():
            break

        time.sleep(interval)


def _check_session_state(stop_event: threading.Event,
                         session_id: int, sess: requests.Session,
                         url: str):
    interval = 1
    endpoint = url + '/api/session/pull?id=%s' % (session_id,)

    while True:
        if stop_event.is_set():
            break

        res = sess.get(endpoint).json()
        if res['error']:
            stop_event.set()
            break

        time.sleep(interval)


def produce_logs(session_id: int, session: requests.Session,
                 url: str, serial: str, filter_uid: int):
    stop_event = threading.Event()
    q = queue.Queue()

    threads = [
        threading.Thread(target=_produce_log_logcat, daemon=True,
                         kwargs={
                             'stop_event': stop_event,
                             'serial': serial,
                             'filter_uid': filter_uid,
                             'q': q,
                         }),
        threading.Thread(target=_produce_log_logcat_sender, daemon=True,
                         kwargs={
                             'stop_event': stop_event,
                             'session_id': session_id,
                             'sess': session,
                             'url': url,
                             'q': q,
                         }),
        threading.Thread(target=_check_session_state, daemon=True,
                         kwargs={
                             'stop_event': stop_event,
                             'session_id': session_id,
                             'sess': session,
                             'url': url,
                         }),
    ]
    for t in threads:
        t.start()

    try:
        log.info('Press Ctrl+C to stop reading...')
        stop_event.wait()

        for t in threads:
            t.join()

    except KeyboardInterrupt:
        stop_event.set()
        for t in threads:
            t.join()

        raise


@device.command('session')
@click.option('-s', '--serial', 'serial', default='')
@click.option('-u', '--url', 'url', default='')
def cmd_device_session(serial, url):
    try:
        while True:
            _cmd_device_session(serial, url)

    except KeyboardInterrupt:
        # If finch user want to stop analyze, break session loop
        pass


def _cmd_device_session(serial, url):
    assert url

    cmdline_prefix = ['adb']
    if serial:
        cmdline_prefix += ['-s', serial]

    sess = requests.Session()

    # establish session
    application_list = util.get_application_list(serial)
    device_info = util.get_device_info(serial)

    res = sess.post(url + '/api/session/register', json={
        'device': device_info,
        'applications': application_list
    }).json()
    log.info('Session started with id=%s (device_id=%s)' % (
        res['id'], res['device_id']
    ))

    # wait server command
    interval = 10.0
    while True:
        cmd_res = sess.get(url + '/api/session/pull?id=%s' % (
            res['id'])).json()
        command = cmd_res['command']
        if command:
            log.info('command: %r' % (command,))
            break

        time.sleep(interval)

    for cmd in command:
        if cmd['action'] == 'download':
            tf = tempfile.NamedTemporaryFile(suffix='.apk', delete=False)
            # download apk
            resp = sess.get(url + cmd['uri'], stream=True)
            assert resp.status_code == 200
            for chunk in resp.iter_content(chunk_size=1024):
                if chunk:
                    tf.write(chunk)

            tf.flush()

            # Check if same package is installed; if so, uninstall current
            # apk and install new one.
            if any([a['name'] == cmd['package'] for a in application_list]):
                subprocess.check_call(cmdline_prefix + [
                    'uninstall', cmd['package']
                ])

            subprocess.check_call(cmdline_prefix + [
                'install', tf.name
            ])

            tf.close()

        elif cmd['action'] == 'upload':
            with tempfile.TemporaryDirectory() as td:
                # extract apk
                apk_path_remote = util.get_application_apk_path(
                    serial)[cmd['package']]
                subprocess.check_call(cmdline_prefix + [
                    'pull', apk_path_remote, td
                ])
                apk_path_local = os.path.join(
                    td, os.path.split(apk_path_remote)[-1])
                assert os.path.isfile(apk_path_local)

                resp = sess.post(url + '/api/report/start', files={
                    'file': open(apk_path_local, 'rb'),
                })
                assert resp.status_code == 200

        elif cmd['action'] == 'execute':
            target_app = cmd['package']

            # before executing make sure device can run given package
            installed_app_uid = -1
            for installed_app in util.get_application_list(serial=serial):
                if installed_app['name'] == target_app:
                    installed_app_uid = installed_app['uid']
                    break

            if installed_app_uid == -1:
                log.error('Cannot find app to execute: %r' % (target_app,))
                break

            # execute application
            subprocess.check_call(cmdline_prefix + [
                'shell', 'am', 'kill', target_app
            ])
            subprocess.check_call(cmdline_prefix + [
                'shell', 'monkey', '-p', target_app,
                '-c', 'android.intent.category.LAUNCHER', '1'
            ])

            produce_logs(session_id=res['id'], session=sess,
                         url=url, serial=serial, filter_uid=installed_app_uid)

        else:
            log.warning('Unknown command received: %r' % (cmd,))

    # stopped
    log.info('Stopped')
    sess.get(url + '/api/session/stop?id=%s' % (res['id']),)


@cli.command('selftest')
def cmd_selftest():
    for program in EXTERNAL_PROGRAMS:
        if shutil.which(program) is None:
            log.warning('Dependency check failed: cannot find program %r' % (
                program,))
            return False

    log.info('All passed!')
    return True


@cli.command('crash')
def crash():
    return 1 / 0


if __name__ == '__main__':
    cli()
