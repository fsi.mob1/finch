NOTICE
=======

Copyright(c) "Finch" contributors, all right reserved.

Before describing, here's a diagram explaining brief architecture and usage of
OSS of Finch project. Gray boxes are our own works, and other small boxes are
OSS.

![License reference and architecture](doc/finch_license_diagram.png)

## Licenses of our works

Our biggest consideration for selecting license is not to violate original open source software license
and obey original authors' perspective. For example, we basically choose Apache 2.0 on almost all platform
repositories because AOSP community prefers Apache 2.0. One exception is
[platform/libcore](https://gitlab.com/fsi.mob1/platform-libcore) because we need to follow license agreements.
If you found any collision or incompatibility about our license selection, please let us know.

- [Finch](https://gitlab.com/fsi.mob1/finch): [Apache 2.0](doc/licenses/Apache_2.0.txt)
- [Finch-Server](https://gitlab.com/fsi.mob1/finch-server): [Apache 2.0](doc/licenses/Apache_2.0.txt)
- [Finch-Manifest](https://gitlab.com/fsi.mob1/manifest): [Apache 2.0](doc/licenses/Apache_2.0.txt)
- Finch-Kernel
  - [kernel/msm](https://gitlab.com/fsi.mob1/kernel-msm): [GPLv2](doc/licenses/GPL-2.0.txt)
  - [kernel-modules/qcacld](https://gitlab.com/fsi.mob1/qcacld): [ISC](doc/licenses/ISC.txt)
- Finch-Platform
  - [platform/bionic](https://gitlab.com/fsi.mob1/platform-bionic): [Apache 2.0](doc/licenses/Apache_2.0.txt)
  - [platform/external/conscrypt](https://gitlab.com/fsi.mob1/platform-external-conscrypt): [Apache 2.0](doc/licenses/Apache_2.0.txt)
  - [platform/frameworks/base](https://gitlab.com/fsi.mob1/platform-frameworks-base): [Apache 2.0](doc/licenses/Apache_2.0.txt)
  - [platform/libcore](https://gitlab.com/fsi.mob1/platform-libcore): [GPLv2](doc/licenses/GPL-2.0.txt)

## Open Source License Announcement

Belows are list of open-source software used by this program. If we accidentally
missed any information, please feel free to contact us, and we will gladly
correct our mistake. To see others repositories related to Finch project, please
visit each repository.

## Finch

### Libraries

- **loguru**: https://pypi.org/project/loguru/
  - Project page: https://github.com/Delgan/loguru
  - Author: Delgan <delgan.py@gmail.com>
  - [MIT License](doc/licenses/MIT.txt)
- **click**: https://pypi.org/project/click/
  - Project page: https://palletsprojects.com/p/click/
  - Author: Armin Ronacher <armin.ronacher@active-4.com>
  - [BSD License (BSD-3-Clause)](doc/licenses/BSD-3-Clause.txt)
- **requests**: https://pypi.org/project/requests/
  - Project page: https://requests.readthedocs.io/
  - Author: Kenneth Reitz <me@kennethreitz.org>
  - [Apache 2.0 License](doc/licenses/Apache_2.0.txt)
- **sentry-sdk**: https://pypi.org/project/sentry-sdk/
  - Project page: https://github.com/getsentry/sentry-python
  - Author: Sentry Team and Contributors <hello@sentry.io>
  - [BSD License](doc/licenses/BSD.txt)
- **paramiko**: https://pypi.org/project/paramiko/
  - Project page: https://paramiko.org/
  - Author: Jeff Forcier <jeff@bitprophet.org>
  - [LGPL 3.0](doc/licenses/LGPL-3.0.txt)
  
### External program (System requirements)

- **Python**: https://www.python.org/
  - Copyright (c) 2001-2021.  Python Software Foundation.
  - [Python Software Foundation License](doc/licenses/PSF.txt)
- **git**: https://git-scm.com/
  - Author: Linus Torvalds
  - [GPL 2.0](doc/licenses/GPL-2.0.txt)
- **cpio**: https://www.gnu.org/software/cpio/
  - Copyright © 2001, 2004, 2005, 2006 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
  - [GPL 3.0](doc/licenses/gpl-3.0.txt)
- **atools**: https://www.nongnu.org/atool/
  - Copyright (C) 2001, 2002, 2003, 2004, 2005, 2007, 2008, 2009, 2011 Oskar Liljeblad
  - [GPL 2.0](doc/licenses/GPL-2.0.txt) or [Later](doc/licenses/gpl-3.0.txt)
- **Android Open Source Project build tools**: https://source.android.com
  - Please See [Content License](https://source.android.com/license) for detailed information about content authors and
    agreements. Each reference of subrepos have its own authors and contributors. 
  - [Apache 2.0](doc/licenses/Apache_2.0.txt)


### Prebuilt executables

- `bin/mkbootimg.*`, `bin/unmkbootimg.*`: **Android Open Source Project** https://source.android.com
  - comes from AOSP's `platform/system/tools/(un)mkbootimg` repository, and we build executable without any modification.
  - [Apache 2.0](doc/licenses/Apache_2.0.txt)
- `prebuilt/crosshatch/pq2a.190305.002`: **Android Linux Kernel** and **related kernel modules** https://android.googlesource.com/kernel/
  - Please see below "Finch-Kernel" repository links for details.

## [Finch-Server](https://gitlab.com/fsi.mob1/finch-server/-/blob/master/NOTICE.md)

## [Finch-Manifest](https://gitlab.com/fsi.mob1/manifest)

## Finch-Platform

  - [platform/bionic](https://gitlab.com/fsi.mob1/platform-bionic)
  - [platform/external/conscrypt](https://gitlab.com/fsi.mob1/platform-external-conscrypt)
  - [platform/frameworks/base](https://gitlab.com/fsi.mob1/platform-frameworks-base)
  - [platform/libcore](https://gitlab.com/fsi.mob1/platform-libcore)

## Finch-Kernel

- [kernel/msm](https://gitlab.com/fsi.mob1/kernel-msm)
- [qcacld](https://gitlab.com/fsi.mob1/qcacld)
