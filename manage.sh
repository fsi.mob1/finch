#!/bin/sh

INSTANCE_ID="i-02672b1bae05339ef"
export AWS_PROFILE=fsec
export AWS_PAGER=


function change_instance_type {
    aws ec2 stop-instances \
        --instance-ids $INSTANCE_ID
    aws ec2 wait instance-stopped \
        --instance-ids $INSTANCE_ID
    aws ec2 modify-instance-attribute \
        --instance-id=$INSTANCE_ID \
        --attribute instanceType --value $1
    aws ec2 start-instances \
        --instance-ids $INSTANCE_ID
    aws ec2 wait instance-running \
        --instance-ids $INSTANCE_ID
}


function usage {
cat <<EOF
Available command:
  - build: Change instance type for build (c5.9xlarge)
  - normal: Change instance type for normal (c5.large)
EOF
}


function test {
    aws ec2 describe-instance-status \
        --instance-id $INSTANCE_ID | grep "\[\]" >/dev/null
    if [ $? -ne 0 ]
    then
        echo "Warning: Instance is running; please shutdown instance."
        exit 1;
    fi
}


function connect {
    ssh AOSP
}


args=("$@")
case ${args[0]} in
build)
    change_instance_type c5.9xlarge
    connect
    ;;
normal)
    change_instance_type c5.large
    connect
    ;;
test)
    test
    ;;
*)
    usage
    ;;
esac;

