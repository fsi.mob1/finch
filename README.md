Finch
=====

## Introduction

Finch is an environment set for Android malware analyzers who sufferes from
anti-debugging, anti-VM, slow and fragile virtual enviroments. This project
provides lab environments using REAL device and REAL TIME logging feature
which can capture malicious activities at system side.

## Installation

To enjoy our project, please see [this tutorial (Korean)](doc/README.md).

## Notice

[Open Source License Announcement](NOTICE.md)

## Related repositories

- [Finch (this repo)](https://gitlab.com/fsi.mob1/finch)
- [Finch-Server](https://gitlab.com/fsi.mob1/finch-server)
- [Finch-Manifest](https://gitlab.com/fsi.mob1/manifest)
- Finch-Kernel
  - [kernel/msm](https://gitlab.com/fsi.mob1/kernel-msm)
  - [kernel-modules/qcacld](https://gitlab.com/fsi.mob1/qcacld)
- Finch-Platform
  - [platform/bionic](https://gitlab.com/fsi.mob1/platform-bionic)
  - [platform/external/conscrypt](https://gitlab.com/fsi.mob1/platform-external-conscrypt)
  - [platform/frameworks/base](https://gitlab.com/fsi.mob1/platform-frameworks-base)
  - [platform/libcore](https://gitlab.com/fsi.mob1/platform-libcore)

## Authors

- SungHyoun Song <decash@fsec.or.kr>
- Jisub Kim <jskimpwn@gmail.com>
- Kyuju Kim <kuyjuim@naver.com>
- Jihyeog Lim <marnitto@gmail.com>

If you want to contact for any reason, prefer contact to marnitto@gmail.com .
