# Finch-Server

[지난 장](README.md)에서는 Android 앱을 실행할 때 분석에 필요한 중요 정보를
기록하는 환경을 구성했습니다. 이번 장에서는 단말기에서 만든 중요 정보를 수집,
가공, 분석하는 기능을 갖춘 Finch-Server 에 대해 설명합니다.

## 준비

### 장치 요구사항

아래 항목들을 원활하게 구동할 수 있는 x86-64 기반 PC가 필요합니다.

  - git, [git-lfs](https://git-lfs.github.com/)
  - [Python 3](https://www.python.org/downloads/)
  - [Docker](https://www.docker.com/) 및 [Docker Compose](https://docs.docker.com/compose/)
  
원활한 서버 기능을 위해 최소 2GB 이상의 메모리가 필요합니다.

## 환경 설정

### 빌드

```
git clone git@gitlab.com:fsi.mob1/finch-server.git
cd finch-server
./restart.sh
```

- Finch보다 시간이 오래 걸립니다. ☕ 
- `docker compose` 명령을 찾지 못한다면 `restart.sh` 파일에서 `docker compose`
  를 `docker-compose`로 변경한 다음 다시 실행해주세요(가운데 `-`가 들어감).

### 상태 점검

```shell
$ docker compose ps
NAME                       COMMAND                  SERVICE             STATUS              PORTS
finch-server_backend_1     "./backend/entrypoin…"   backend             running             0.0.0.0:5000->5000/tcp, :::5000->5000/tcp
finch-server_chain_1       "/tini -- python3 -m…"   chain               running             5000/tcp
finch-server_database_1    "docker-entrypoint.s…"   database            running             5432/tcp
finch-server_kafka_1       "/etc/confluent/dock…"   kafka               running             9092/tcp
finch-server_zookeeper_1   "/etc/confluent/dock…"   zookeeper           running             2181/tcp, 2888/tcp, 3888/tcp 
```

- 하나라도 running 상태가 아니라면 `./restart.sh`를 다시 실행해보시고,
  그래도 해결되지 않는다면 이슈를 남겨주세요.

## 실행

웹 브라우저로 http://localhost:5000 에 접속해주세요. 사이트가 제대로 보인다면,
이제 [Finch와 Finch-Server를 이용한 앱 분석](USAGE.md) 단계로 넘어가겠습니다.
