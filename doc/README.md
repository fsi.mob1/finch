# Finch

이 문서에서는 Finch 프로젝트를 이용하는 방법에 대해 설명합니다.


## 준비

### 장치 요구사항

- Android 9.0 Pie를 지원하는 Google 휴대폰 (예: Google Pixel 3 XL (crosshatch)) 
    - 기기 플래싱을 위해선 Bootloader 및 OEM 잠금 해제가 필요합니다. **OEM 해제가 불가능한 장치는 이용하실 수 없습니다**(예: Verizon). 
    - Google 휴대폰 목록은 [여기](https://source.android.google.cn/setup/start/build-numbers?hl=ko#source-code-tags-and-builds) 를 참고해주세요.
    - 현재 버전에서는 Google Pixel 3 XL만 지원합니다.
- Host PC와 장치는 USB 케이블로 연결해야 합니다.

### 프로젝트 실행 요구사항

아래 항목들을 원활하게 구동할 수 있는 x86-64 기반 PC가 필요합니다.

  - git, [git-lfs](https://git-lfs.github.com/)
  - [Python 3](https://www.python.org/downloads/)
  - [Docker](https://www.docker.com/) 및 [Docker Compose](https://docs.docker.com/compose/)
  - [Android SDK](https://developer.android.com/studio)
  - [atool](https://www.nongnu.org/atool/)

## 환경 설정

```
git clone git@gitlab.com:fsi.mob1/finch.git
cd finch
git lfs install
git lfs pull
pip3 install -r requirements.txt
python3 finch.py selftest
```

selftest 실행 결과 "All passed"가 보여야 합니다.

### 커스텀 플랫폼 내려받기

Finch에서는 환경 구성을 위해 장치에 맞는 커스텀 플랫폼 파일을 필요로 합니다.
이용하시는 장치에 맞는 파일을 `output/` 디렉터리에 내려받아주세요.

|장치명             | 파일  | 업데이트 날짜 | 해시 검증 |
|-------------------|-------|---------------|-----------|
| Google Pixel 3 XL | [system.img.raw](https://drive.google.com/open?id=1-_MuHhDyRJ8IvEHrmY4UYO9m5hWrLRlN) | 2021-10-12 | [system.img.raw.sha256](https://drive.google.com/open?id=1aJOBtOPf9iCf1tCgLqDCdqPBFMLp70G_) |  


## 장치 환경 설정

휴대폰을 adb USB debugging 및 fastboot unlock 상태로 설정하시고 아래 절차대로 진행해주세요.

[Google 장치용 플래싱 환경 설정 방법](https://developers.google.com/android/images#instructions)


### 초기화

아래 "플랫폼 설치" 단계부터 정상 동작하지 않아 처음부터 다시 하고 싶으시다면 여기부터 진행하시면 됩니다.

```shell
python3 finch.py device reset
```

정상 실행됐다면 이 단계에서 장치가 공장 초기화된 상태일 것입니다. 아래 사항에 유의해서 장치를 설정해주세요.

- 기기 잠금 비밀번호/패턴은 **설정하지 않음**
- 개발자 모드 설정
  - 설정 - 휴대전화 정보 - 소프트웨어 정보 - 빌드 번호 항목을 5회 이상 연타 - 개발자 모드 활성화
  - 설정 - 시스템 - 고급 - 개발자 옵션
    - USB 디버깅 설정
      - USB fingerprint 신뢰 여부를 물어보면 앞으로도 신뢰 체크박스를 체크한 상태로 수락
      - 신뢰 여부를 더 물어보지 않을 때까지 USB 연결을 뺐다 끼면서 확인
    - 로거 버퍼 크기: 16MB 이상 (지원 옵션 중 가장 큰 값으로 설정)
- USB 연결 확인: 기기 내 장치 연결 모드는 "데이터 연결"로 설정하시는 걸 권장합니다.

### 플랫폼 설치

```shell
python3 finch.py device flash platform
```

### 커널 설치

```shell
python3 finch.py device flash kernel
```

### 테스트

```shell
adb logcat | grep HOOKA
```

HOOKA로 시작하는 항목이 보인다면 설치가 정상적으로 진행된 것입니다. 


## 실행

```shell
python3 finch.py
```

이제 Finch 장치를 이용해 로그를 수집할 준비가 끝났습니다. 수집 서버에 관한 자세한 내용은 [Finch-Server 설정](SETUP_SERVER.md)을 참조해주세요.
