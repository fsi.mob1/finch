# Usage

[지난 장](SETUP_SERVER.md)까지 잘 따라오셨다면 Finch, Finch-Server를 이용할
준비가 된 상황입니다. 이제부터는 Finch 프로젝트를 이용해 앱을 분석하는 방법을
예제를 통해 설명해드리겠습니다. 

## 예제 환경

분석에 사용할 예제 환경은 다음과 같습니다. 일부 환경이 다르더라도 Finch 실행에는
큰 문제가 없습니다. 이용하시는 환경에서 잘 동작하지 않는다면 이슈를 남겨주세요.

- Finch 장치: Google Pixel 3 XL
- Finch와 Finch-Server는 같은 PC (macOS, intel)에서 실행
- 분석 대상 apk
  - [F-Droid.apk](https://www.f-droid.org/)
  
예제 환경 구성을 위해 장치에 F-Droid를 설치하겠습니다. 위 링크에서 F-Droid를
내려받고 아래 명령어를 실행해주세요.

```shell
adb install ./F-Droid.apk
```

# 기본 사용법: 앱 분석 및 실행

지금부터는 Finch와 Finch-Server를 사용하는 기본적인 방법을 설명합니다.

## Finch와 Finch-Server 연결

1. Finch-Server 디렉터리에서 `./restart.sh` 쉘을 실행해 서비스를 기동합니다.
2. Finch 장치를 USB로 연결하고, Finch 디렉터리에서 아래 명령을 실행합니다.

```
python3 finch.py device session -u http://localhost:5000
```

3. 웹 브라우저로 http://localhost:5000 에 접속해서 `Select Target`에 장치
   정보가 보이는지 확인해주세요.

`Select Target` 메뉴에서 분석에 사용할 장치와, 장치에 설치되어 있는 제3자
애플리케이션 목록을 볼 수 있습니다. 예제 환경을 제대로 구성하셨다면 목록에서
"F-Droid"를 볼 수 있습니다.

![](./finch_server_select_target.png)

## 애플리케이션 실행 및 분석 명령

위 화면에서 F-Droid를 분석하려면, 왼쪽에 있는 F-Droid 체크박스를 눌러 체크
모양을 만들고, 오른쪽 아래 "Start Analyze" 버튼을 눌러주세요. 잠시 기다리시면
장치에서 F-Droid를 실행하고, 웹 브라우저는 "Dynamic Analysis" 메뉴로 이동한 다음
실시간으로 분석 로그를 보실 수 있습니다.

![](./finch_server_dynamic_analysis.png)

서비스에서 보여주는 로그 세부 정보는 아래와 같습니다.

- 로그 발생 시간
- 분류 / 패턴: 미리 정의한 악성 행위 패턴에 해당하는 경우, 악성 행위 분류와
  악성 행위 패턴 설명
- 호출자: 로그가 발생한 위치. 주로 Android Framework 내 특정 method를
  지칭합니다.
- Payload: 세부 로그 정보. 호출자 종류에 따라 다른 정보가 나옵니다.

분석을 중단하고 싶으시다면, 우측 아래 "Stop Analysis" 버튼을 눌러주세요.

### 로그 검색

"Search"와 돋보기 모양이 있는 곳에 검색어를 입력하시면 검색어에 맞는 로그만
보실 수 있습니다.

- 은닉성 악성코드 패턴 로그를 보고 싶으시다면: `HIDING`
- `example.com` 도메인 접속 기록이 궁금하시다면: `example.com`

# 응용: 로그 탐지 패턴 추가

새로운 악성코드 패턴을 발견하시거나 특정 항목을 강조하고 싶으시다면,
`finch-server/chain/init_model.yml` 파일을 통해 패턴을 정의할 수 있습니다. 
예를 들어, 은닉형 악성코드 패턴 중 앱 아이콘 숨기기 기능을 실행하는 코드는
다음과 같이 호출합니다.

```java
PackageManager p = getPackageManager();
ComponentName componentName = new ComponentName(this, com.apps.MainActivity.class); 
p.setComponentEnabledSetting(componentName,PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
```

Finch는 `PackageManager.setComponentEnabledSetting()` 호출이 발생할 때마다 호출
내용을 Finch-Server로 전달합니다. 이 메서드의 매개변수 중 아이콘 숨김을 위해
사용하는 매개변수는 `COMPONENT_ENABLED_STATE_DISABLED`이므로, 이를 종합하면
아래처럼 패턴을 만들 수 있습니다.

```yaml
pattern:
  - caller: "PackageManager.setComponentEnabledSetting"
    payload_filter_regex: ".*COMPONENT_ENABLED_STATE_DISABLED.*"
    category: "HIDING"
    title: "아이콘 숨기기"
```

패턴을 업데이트했다면 `finch-server/restart.sh` 를 이용해 서버를 재시작해줘야
변경 내용을 반영할 수 있습니다.
