# -*- coding: utf-8 -*-

import os
import hashlib
import subprocess
import time
import gzip
from xml.etree import ElementTree as ET

import requests
from loguru import logger as log


def extract_linux_version(path: str) -> str:
    start_signature = b'Linux version '

    with open(path, 'rb') as f:
        data = f.read()
        start_index = data.index(start_signature)
        end_index = data.index(b' ', start_index+len(start_signature))
        ret = data[start_index:end_index].decode().strip().split('-')[0]
        return ret


def get_android_image_url(device_code: str, version: str) -> (str, str):
    """
    @param device_code Device code (ex. crosshatch)
    @param version Version code (ex. pq2a.190305.002)
    @return url, hash
    """
    needle = f'https://dl.google.com/dl/android/aosp/' \
             f'{device_code}-{version}-factory-'
    page = requests.get(
        'https://developers.google.cn/android/images?hl=ko').text
    if needle not in page:
        log.error(f'Android image not found: device={device_code} ' \
                  f'version={version}')
        return '', ''

    url = page[page.index(needle):]
    file_hash = url[url.index('<td>')+4:]
    file_hash = file_hash[:file_hash.index('</td>')]
    url = url[:url.index('"')]
    assert url.startswith('https://')
    assert '.zip?' in url
    return url, file_hash


def download_file(url: str, local_path: str, verify_hash=''):
    size = 0
    hasher = hashlib.sha256()

    with requests.get(url, stream=True) as r:
        r.raise_for_status()
        with open(local_path, 'wb') as f:
            for chunk in r.iter_content(chunk_size=8192):
                # If you have chunk encoded response uncomment if
                # and set chunk_size parameter to None.
                f.write(chunk)
                hasher.update(chunk)
                size += len(chunk)

    if verify_hash:
        if hasher.hexdigest() == verify_hash:
            log.info('Hashes match.')
        else:
            log.warning('Hashes mismatch.')

    return size


def start_fastboot_mode(serial=''):
    # If target is connected via adb, reload with fastboot mode
    devices_in_adb = [
        s.split()[0]
        for s in subprocess.check_output(['adb', 'devices'])
                           .decode().split('\n')[1:]
        if 'device' in s
    ]

    if len(devices_in_adb) == 1:
        serial = devices_in_adb[0]
    elif len(devices_in_adb) > 1 and serial not in devices_in_adb:
        log.error('Please specify serial to flash. Available devices: %r' % (
            devices_in_adb))
        return

    if serial in devices_in_adb:
        log.info('Switching to fastboot mode')
        subprocess.check_call(['adb', '-s', serial, 'reboot-bootloader'])

    # Check our target device is in fastboot mode
    max_retry_count = 10
    target_found = False
    for _ in range(max_retry_count):
        time.sleep(1)
        fastboot_devices_result = subprocess.check_output([
            'fastboot', 'devices']).decode().strip()
        if not fastboot_devices_result:
            continue

        devices_in_fastboot = [
            s.split()[0]
            for s in fastboot_devices_result.split('\n')
        ]

        if serial and serial in devices_in_fastboot:
            target_found = True
            break
        elif len(devices_in_fastboot) == 1:
            serial = devices_in_fastboot[0]
            log.info('Using serial %r' % (serial,))
            target_found = True
            break

    assert target_found
    return serial


def get_device_info(serial=''):
    # FIXME: Are there any elegant ways making client knows what information
    #        should be sent to determine unique device info on handling
    #        `backend/api/log/start`?
    ret = {}

    def query(content: str):
        cmdline = ['adb']
        if serial:
            cmdline += ['-s', serial]

        cmdline += ['shell']
        p = subprocess.Popen(cmdline,
                             stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        p.stdin.write(content.encode()+b'\n')
        p.stdin.flush()
        ret = b''
        while True:
            c = p.stdout.read(1)
            if c in b'\r\n':
                break

            ret += c

        p.kill()
        return ret.decode() or 'X'

    ret['serial'] = query('getprop ro.serialno')
    ret['platform_build_id'] = query('getprop ro.build.id')
    ret['platform_ver'] = query('getprop ro.build.version.release')
    ret['product_brand'] = query('getprop ro.product.brand')
    ret['product_device'] = '%s %s' % (
        query('getprop ro.product.manufacturer'),
        query('getprop ro.product.model')
    )
    ret['kernel_ver'] = query('uname -r')

    return ret


def get_application_apk_path(serial=''):
    cmd_prefix = ['adb']
    if serial:
        cmd_prefix += ['-s', serial]

    # get package lists we are interested in (3rd party app)
    cmd = cmd_prefix + ['shell', 'pm', 'list', 'packages', '-3', '-f']
    output = subprocess.check_output(cmd).decode('utf-8').strip()
    packages = {}
    if output:
        for o in output.split('\n'):
            print(repr(o))
            apk_path, name = o.split(':', 1)[-1].rsplit('=', 1)
            packages[name] = apk_path

    return packages


def get_application_list(serial=''):
    cmd_prefix = ['adb']
    if serial:
        cmd_prefix += ['-s', serial]

    # get package lists we are interested in (3rd party app)
    packages = get_application_apk_path(serial)
    cmd = cmd_prefix + ['shell', 'sh', '-c', 'cat\\ /data/system/packages.xml']
    tree = ET.fromstring(subprocess.check_output(cmd).decode('utf-8'))
    ret = []
    for p in tree.findall('.//package'):
        if p.attrib['name'] not in packages.keys():
            continue

        name = p.attrib['name']
        uid = int(p.attrib['userId'])
        ret_elem = {
            'name': name,
            'uid': uid,
            'hash': subprocess.check_output(
                cmd_prefix + ['shell', 'sha256sum', packages[name]])
                .split()[0].decode('utf-8')
        }
        ret.append(ret_elem)

    return ret


def mock_logcat(path: os.PathLike):
    assert os.path.exists(path)

    class MockProcessStdout(object):
        def __init__(self, filename):
            self.f = gzip.open(filename, 'rb')

        def read(self, size):
            return self.f.read(size)

    class MockProcess:
        def __init__(self, filename):
            self.stdout = MockProcessStdout(filename)

        def kill(self):
            pass

    return MockProcess(path)
